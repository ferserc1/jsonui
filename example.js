
function onload() {
	var dom = jsonui.dom.create(
	{tagName:'div',"class":"parentNode",
		childs:[
			{tagName:"div","id":"elem1","class":"c1",childs:[
				{tagName:"a","href":"http://www.upv.es","target":"_blank",innerText:"UPV"},{tagName:"br"},
				{tagName:"a","href":"http://polimedia.upv.es/catalogo","target":"_blank",innerText:"Catálogo Polimedia"},{tagName:"br"},
				{tagName:"a","href":"http://polimedia.upv.es","target":"_blank",innerText:"Polimedia"}
			]},
			{tagName:"div","id":"elem2","class":"c1",childs:[
				{tagName:"img",
						"src":"http://www.upv.es/imagenes/marcaUPV_home61.png",
						"alt":"upvlogo",
						"click":function(event) { alert("Hello World")}}
			]}
		]
	});
	document.body.appendChild(dom);
	
	var user = jsonui.form.inputText("user","Usuario","",null,{factory:"TextInputFactory",type:"email",message:"Pon un email, imbecil"});
	var pass1 = jsonui.form.inputPassword("pass","Contraseña","",null,[{factory:"TextInputFactory",type:"required",message:"Tienes que meter una contraseña por cojones"},
																	   {factory:"TextInputFactory",type:"regexp",regexp:"^\\d+$",message:"pon solo numericos, macho, que no te enteras"}]);
	var pass2 = jsonui.form.inputRepeatPassword(pass1,"Repetir");
	var options = [{value:"option1",label:"Caro"},{value:"option2",label:"Carísimo"},{value:"option3",label:"Descomunal"}];
	var selectFactory = {factory:"SelectInputFactory",type:"hint",
							messages:{"option1":"Buena opción",
									  "option2":"Bien, si te sobra la pasta no seré yo quien te juzgue",
									  "option3":"Míratelo bien, que creo que tienes algún problemilla en la chepa"}};
	var select = jsonui.form.comboBox('payment',"Tipo de pago",options,"option2",null,[selectFactory]);
	var accept = jsonui.form.checkBox("accept","Acepto vender mi alma",false,"",[{factory:"CheckBoxFactory",type:"required",message:"Tienes que aceptar vender tu alma"}]);
	var submit = {tagName:'input',"type":"submit"}
	var form = {tagName:"form","action":"#",childs:[user,pass1,pass2,select,accept,submit],metadata:[
						{factory:"FormFactory",ajaxform:true,
							onsubmit:function() {
								alert("Enviar formulario");
							},
							onfail:function() {
								alert("Hay campos mal, capullo");
							}
						}]};
	var formDom = jsonui.dom.create(form);
	document.body.appendChild(formDom);
}