# jsonui

jsonui is a Javascript library that allows you to create DOM trees and rich user interfaces using a JSON (JavaScript Object Notation) definition.



## JSON structure
To specify a DOM tree using JSON there are only three simple rules:

1. A DOM node corresponds with a JSON object, so the root of your JSON specification must be an object, and not an array.
2. Almost all the attributes of the JSON object will be converted into element attributes.
3. There are only four reserved attributes that can not be converted into DOM attributes:
	1. tagName: is used to specify the element tag name.
	2. innerText: will be converted into the innerHTML DOM node.
	3. childs: this attribute must contain an array of other JSON-DOM elements, and will be converted into the DOM element's child elements.
	4. metadata: this attribute must contain the definition of other data that jsonui will use to build rich UI elements.

Only the 'tagName' attribute is required.

## Defining a simple DOM tree

To define a DOM tree, you only need to create the equivalent JSON object, following the rules specified above. By convention, we'll use quoted attribute names to specify the DOM element attributes, and non-quoted attribute names to specify the reserved attributes.

	var myDomTree = { tagName:"div", "class":"myDomTree",
						childs:[
							{ tagName:"a", "href":"http://www.google.com", innerText:"Search on Google" }, {tagName:"br"},
							{ tagName:"a", "href":"http://www.upv.es", innerText:"Universitat Politècnica de València" }
						] };
						
	document.body.appendChild(jsonui.dom.create(myDomTree));

This will be converted into:

	<div class="myDomTree">
		<a href="http://www.google.com">Search on Google</a>
		<br/>
		<a href="http://www.upv.es">Universitat Politècnica de València</a>
	</div>


## Metadata and factory objects

The metadata object is used to provide extra data to jsonui that allows to build rich UI elements and complex behaviours. A metadata object must contain at least a 'factory' name. This factory have the role of parse the rest of the metadata object's attributes and add the extra behaviours to the pure HTML field.

You can define extra factories and build your own metadata objects to extend jsonui.

### Factory name

The only required attributes

