var jsonui = {}

if (console===undefined) console = {log:function(msg) {}}

jsonui.dictionary = {
	translateFunction:base.dictionary.translate,

	translate:function(key) {
		if (typeof(this.translateFunction)=='function') {
			return this.translateFunction(key);
		}
		else {
			return key;
		}
	}
}

jsonui.factory = {
	DefaultFactory:{
		build:function(domNode,metadata) {
			return domNode;
		}
	}
}

jsonui.factory.FactoryBase = Class.create({
	build:function(domNode,metadata) {

	},

	checkField:function(metadata) { return true; }
});

jsonui.utils = {
	checkEmail:function(text) {
		var regExp = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
		return this.checkFormat(text,regExp);
	},

	checkNumber:function(text) {
		var regExp = /^(\+|\-)*\d+(.\d+)?$/;
		return this.checkFormat(text,regExp);
	},

	checkFormat:function(text,regExp) {
		return regExp.test(text);
	}
}

jsonui.dom = {
	create:function(obj) {
		if (typeof(obj)!='object') {
			throw "jsonui.dom.create(): the supplied parameter is not an object";
		}
		if (typeof(obj.length)=='number') {
			throw "jsonui.dom.create(): The supplied parameter must be an object and not an array";
		}
		return this.getDomNode(obj);
	},

	getDomNode:function(jsonObj) {
		if (typeof(jsonObj)=='object') {
			var completeFactory = null;
			if (jsonObj['tagName']===undefined) jsonObj.tagName = 'div';
			var elem = document.createElement(jsonObj.tagName);
			elem.jsonui = { constructor:jsonObj, metadata:[] };
			for (var key in jsonObj) {
				var value = jsonObj[key];
				if (key=='tagName') continue;
				if (key=='children' && typeof(value)=='object' && typeof(value.length)=="number") {
					this.appendNodesInArray(elem,value);
				}
				if (key=='innerText' && (typeof(value)=='string' || typeof(value)=='number')) {
					elem.innerHTML += value;
				}
				else if (typeof(value)=='function'){
					if (key=='resize') {
						$(window).bind('resize',value);
					}
					else {
						$(elem).bind(key,value);
					}
				}
				else if (key=='data') {
					elem.data = value;
				}
				else if (key=='metadata') {
					var factoryData = jsonObj[key];
					if (factoryData.length) {
						for (var i=0;i<factoryData.length;++i) {
							elem.jsonui.metadata.push(factoryData[i]);
							if (factoryData[i].factory!='OnCompleteFactory')
								this.buildMetadata(elem,factoryData[i]);
							else
								completeFactory = factoryData[i];

						}
					}
					else {
						elem.jsonui.metadata.push(factoryData);
						this.buildMetadata(elem,factoryData);
					}
				}
				else if (key!='children' && key!='tagName' && key!='innerText' && key!='metadata'){
					elem.setAttribute(key,value);
				}
			}
			if (completeFactory) {
				this.buildMetadata(elem,completeFactory);
			}
			return elem;
		}
		return null;
	},

	buildMetadata:function(domElem,factoryData) {
		if (jsonui.factory[factoryData.factory]!==undefined) {
			var factory = new jsonui.factory[factoryData.factory];
			factory.build(domElem,factoryData);
		}
	},

	appendNodesInArray:function(domNode,array) {
		if (typeof(domNode)=='object' && typeof(array)=='object' && array.length>0) {
			for (var i=0;i<array.length;++i) {
				var child = this.getDomNode(array[i]);
				if (child) {
					domNode.appendChild(child);
				}
			}
		}
	}
}

jsonui.findId = function(jsonData,id) {
	if (jsonData.id==id) {
		return jsonData;
	}
	else if (typeof(jsonData.children)=="object" && jsonData.children.length>0) {
		var result = null;
		for (var i=0; result==null && i<jsonData.children.length; ++i) {
			result = jsonui.findId(jsonData.children[i],id);
		}
		return result;
	}
	else {
		return null;
	}
}

jsonui.findClass = function(jsonData,className,allowMultipleClasses) {
	var result = null;

	if (allowMultipleClasses===true) {
		if ('class' in jsonData) {
			var classes = jsonData["class"].split(" ");
			for (var i = classes.length - 1; i >= 0; i--) {
				var jsonClasss = classes[i];
				if (jsonClasss==className) {
					result = [ jsonData ];
				}
			}
		}
	}
	else if (jsonData["class"]==className) {
		result = [ jsonData ];
	}

	if (result) return result;

	if (typeof(jsonData.children)=="object" && jsonData.children.length>0) {
		var result = [];
		for (var i=0; i<jsonData.children.length; ++i) {
			var items = jsonui.findClass(jsonData.children[i],className,allowMultipleClasses);
			if (items.length>0) result = result.concat(items);
		}
		return result;
	}
	else {
		return [];
	}
}

jsonui.findTag = function(jsonData,tagName) {
	var result = [];

	if ((!jsonData["tagName"] && tagName=="div") ||
		(jsonData["tagName"]==tagName)) {
		result = [ jsonData ];
	}

	if (typeof(jsonData.children)=="object" && jsonData.children.length>0) {
		var childrenResult = []
		for (var i=0; i<jsonData.children.length; ++i) {
			var items = jsonui.findTag(jsonData.children[i],tagName);
			if (items.length>0) childrenResult = childrenResult.concat(items);
		}
		result = result.concat(childrenResult);
	}
	return result;
}

jsonui.tag = function(tagName,data) {
	data.tagName = tagName;
	if (!data.children) data.children = [];
	return data;
}
jsonui.$h1 = function(data) { return jsonui.tag('h1',data); }
jsonui.$h2 = function(data) { return jsonui.tag('h2',data); }
jsonui.$h3 = function(data) { return jsonui.tag('h3',data); }
jsonui.$h4 = function(data) { return jsonui.tag('h4',data); }
jsonui.$h5 = function(data) { return jsonui.tag('h5',data); }
jsonui.$h6 = function(data) { return jsonui.tag('h6',data); }
jsonui.$h7 = function(data) { return jsonui.tag('h7',data); }
jsonui.$p = function(data) { return jsonui.tag('p',data); }
jsonui.$span = function(data) { return jsonui.tag('span',data); }
jsonui.$div = function(data) { return jsonui.tag('div',data); }
jsonui.$form = function(data) { return jsonui.tag('form',data); }
jsonui.$input = function(data) { return jsonui.tag('input',data); }
jsonui.$img = function(data) { return jsonui.tag('img',data); }
jsonui.$a = function(data) { return jsonui.tag('a',data); }
// TODO: Do the same with the rest of the tags

jsonui.form = {
	fieldContainerClass:'fieldContainer',
	hintClass:'hint',

	fieldContainer:function(subclass,id,metadata) {
		if (subclass===undefined) subclass = '';
		else subclass = ' ' + subclass;
		var container = {tagName:'div',"class":this.fieldContainerClass + subclass,children:[]}
		if (id) container["id"] = id + "_container";
		if (typeof(metadata)=='string') {
			try {
				metadata = JSON.parse(metadata);
			}
			catch (e) {
				console.log('WARNING (jsonui): could not parse metadata field. Metadata must be a valid json string or an object');
				metadata = null;
			}
		}
		container.metadata = metadata ? metadata:[];
		return container;
	},

	inputField:function(fieldData,label,className,metadata) {
		if (className==undefined) className = "field";
		else className = className;
		var cont = this.fieldContainer(className,fieldData.id,metadata);
		label = jsonui.dictionary.translate(label) + ':';
		var labelData = {tagName:'label',"for":fieldData.id,innerText:label,"class":className + " label"};
		var hintData = {tagName:'span',"class":this.hintClass}
		fieldData["class"] = className + " field";
		cont.children.push(labelData);
		cont.children.push(fieldData);
		cont.children.push(hintData);
		return cont;
	},

	inputText:function(id,label,value,className,metadata) {
		if (value==undefined) value = "";
		var inputData = {tagName:'input',"type":"text","value":value,"id":id,"name":id}
		return this.inputField(inputData,label,className,metadata);
	},

	inputPassword:function(id,label,value,className,metadata) {
		if (value==undefined) value = "";
		var inputData = {tagName:'input',"type":"password","value":value,"id":id,"name":id}
		var cont = this.inputField(inputData,label,className,metadata);
		return cont;
	},

	inputRepeatPassword:function(otherPassField,label,className) {
		if (otherPassField && otherPassField.children[1] && otherPassField.children[1].tagName=='input') {
			var otherInput = otherPassField.children[1];
			var value = otherInput.value;
			var id = otherInput.id + "_repeat";
			var metadata = {factory:"TextInputFactory",type:"repeat_password",otherFieldId:otherInput.id};
			if (value==undefined) value = "";
			var inputData = {tagName:'input',"type":"password","value":value,"id":id,"name":id}
			return this.inputField(inputData,label,className,metadata);
		}
		return null;
	},

	checkBox:function(id,label,checked,className,metadata) {
		if (className==undefined) className = "field";
		var cont = this.fieldContainer(className,id,metadata);
		//var cont = {tagName:'div',"class":this.fieldContainerClass + " " + className,"id":id + "_container",children:[]}
		var label = {tagName:'label',"for":id,innerText:label,"class":className + " label"}
		var field = {tagName:'input',"type":"checkbox","name":id,"id":id,"class":className};
		if (checked) field["checked"] = "checked";
		var hint = {tagName:'span',"class":this.hintClass}
		cont.children.push(label);
		cont.children.push(field);
		cont.children.push(hint);
		return cont;
	},

	// options: [{label:"Label 1",value:"value1"},{label:"Label 2",value:"value2"}]
	comboBox:function(id,label,options,selected,className,metadata) {
		var inputData = {tagName:'select',"id":id,"name":id,children:[]}
		for (var i=0;i<options.length;++i) {
			var opt = options[i];
			var option = {tagName:"option","value":opt.value,innerText:opt.label}
			if (opt.value==selected) option.selected = "selected";
			inputData.children.push(option);
		}
		return this.inputField(inputData,label,className,metadata);
	}
}

jsonui.factory.FormFieldFactory = Class.create(jsonui.factory.FactoryBase,{
	build:function(domNode,metadata) {
		var children = domNode.childNodes;
		for (var i=0;i<children.length;++i) {
			var child = children[i];
			if (child.tagName=='LABEL') {
				metadata.label = child;
			}
			else if (child.tagName=='INPUT') {
				metadata.input = child;
			}
			else if (child.tagName=='SPAN') {
				metadata.span = child;
			}
			else if (child.tagName=='SELECT') {
				metadata.select = child;
			}
			else if (child.tagName=='TEXTAREA') {
				metadata.textArea = child;
			}
		}
		metadata.factoryInstance = this;
		this.buildFields(metadata);
	},

	buildFields:function(metadata) {
		var field = metadata.input ? metadata.input:metadata.select ? metadata.select:metadata.textArea;
		if (field && metadata.type=='disabled') {
			field.disabled = "disabled";
		}
		if (field && metadata.type=='event') {
			if (typeof(metadata.change)=="function") {
				field.metadata = metadata;
				field.addEventListener("change",function(event) {
					this.metadata.change(event,this.metadata);
				});
			}
		}
		else if (field) {
			field.addEventListener("change",function(event) {
				for (var i=0;i<this.parentNode.jsonui.metadata.length;++i) {
					var metadata = this.parentNode.jsonui.metadata[i];
					if (!metadata.factoryInstance.checkField(metadata)) break;
				}
			})
			if (typeof(this[metadata.type])=='function') {
				this.checkFunction = this[metadata.type];
			}
		}
	},

	checkField:function(metadata) {
		return (typeof(this.checkFunction)=="function") ? this.checkFunction(metadata):true;
	},

	setHint:function(metadata,text,status) {
		text = metadata.message ? metadata.message:text;
		metadata.span.innerHTML = status ? "":jsonui.dictionary.translate(text);
		metadata.span.className = jsonui.hintClass + (status ? '':' error');
		return status;
	}
});

jsonui.factory.TextInputFactory = Class.create(jsonui.factory.FormFieldFactory, {
	email:function(metadata) {
		return this.setHint(metadata,"Invalid e-mail format (ex: foo@server.com)",jsonui.utils.checkEmail(metadata.input.value));
	},

	regexp:function(metadata) {
		if (typeof(metadata.regexp)=="string") metadata.regexp = RegExp(metadata.regexp);
		return this.setHint(metadata,"Invalid format",jsonui.utils.checkFormat(metadata.input.value,metadata.regexp));
	},

	repeat_password:function(metadata) {
		var otherField = document.getElementById(metadata.otherFieldId);
		if (!otherField) {
			metadata.message = null;
			return this.setHint(metadata,"WARNING: other passowrd field not found",false);
		}
		else {
			return this.setHint(metadata,"Passwords does not match",otherField.value==metadata.input.value);
		}
	},

	required:function(metadata) {
		return this.setHint(metadata,"This field is required",metadata.input.value!="");
	}
});

jsonui.factory.CheckBoxFactory = Class.create(jsonui.factory.TextInputFactory,{
	required:function(metadata) {
		return this.setHint(metadata,"This field is required",metadata.input.checked);
	}
});

jsonui.factory.SelectInputFactory = Class.create(jsonui.factory.FormFieldFactory, {
	hint:function(metadata) {
		var messages = metadata.messages;
		var selectField = metadata.select;
		var value = selectField.options[selectField.selectedIndex].value
		var msg = jsonui.dictionary.translate(metadata.messages[value]);
		metadata.span.innerHTML = (msg) ? msg:"";
		return true;
	}
});

jsonui.factory.FormFactory = Class.create(jsonui.factory.FactoryBase,{
	build:function(domNode,metadata) {
		domNode.metadata = metadata;
		metadata.factoryInstance = this;
		if (metadata.ajaxform) {
			domNode.onsubmit = function(event) {
				this.metadata.factoryInstance.onSubmitFunction(this,this.metadata);
				return false;
			}
		}
		else {
			domNode.addEventListener('submit',function(event) {
				this.metadata.factoryInstance.onSubmitFunction(this,this.metadata);
			});
		}

	},

	onSubmitFunction:function(form,formMetadata) {
		var formData = {}
		var inputs = form.getElementsByTagName("input");
		var selects = form.getElementsByTagName("select");
		var textAreas = form.getElementsByTagName("textarea");
		var status = true;
		for (var i=0;i<inputs.length;++i) {
			var input = inputs[i];
			if (input.type=='button' || input.type=='submit') continue;
			var metadata = input.parentNode.jsonui.metadata;
			if (input.type=='checkbox') formData[input.name] = input.checked;
			else formData[input.name] = input.value;
			if (metadata) {
				for (var j=0;j<metadata.length;++j) {
					if (metadata[j].factoryInstance && typeof(metadata[j].factoryInstance.checkFunction)=="function") {
						status = status && metadata[j].factoryInstance.checkFunction(metadata[j]);
					}
				}
			}
		}

		// TODO: select && textarea, y demás
		for (var i=0;i<selects.length;++i) {
			var select = selects[i];
			var metadata = select.parentNode.jsonui.metadata;
			formData[select.name] = select.options[select.selectedIndex].value;
		}



		if (status && typeof(formMetadata.onsubmit)=='function') {
			formMetadata.onsubmit(formMetadata.userData,formData);
		}
		else if (!status && typeof(formMetadata.onfail)=='function') {
			formMetadata.onfail(formMetadata.userData,formData);
		}
	}
});

jsonui.ModalForm = Class.create({
	viewContainerId:'modalFormViewContainer',
	container:null,
	options:null,
	viewContainer:null,

	initialize:function(content,options) {
		this.options = {
			closeButton:true,
			closeOnClickBackground:false
		}
		if (typeof(options)=="object") {
			for (var key in options) {
				this.options[key] = options[key];
			}
		}
		jsonui.ModalForm.close();
		jsonui.ModalForm.g_CurrentForm = this;
		var container = this.createWindow(content);
		this.container = jsonui.dom.create(container);
		document.body.appendChild(this.container);
		this.viewContainer = $('#' + this.viewContainerId)[0];
	},

	createWindow:function(content) {
		var modalBackground = {"class":"modalBackground",children:[]}
		var modalFrame = {"class":"modalFrame",children:[]}
		if (this.options.closeButton) {
			modalFrame.children.push({"class":"modalFrameCloseButton",innerText:"X",click:function(event) { jsonui.ModalForm.close(); }});
		}
		var viewContainer = {"class":"viewContainer","id":this.viewContainerId,children:[content]}
		modalBackground.children.push(modalFrame);
		modalFrame.children.push(viewContainer);
		if (this.options.closeOnClickBackground) {
			modalBackground.click = function(event) {
				jsonui.ModalForm.close();
			}
		}
		return modalBackground;
	},

	setContent:function(content) {
		this.setDomContent(jsonui.dom.create(content));
	},

	appendContent:function(content) {
		this.appendDomContent(jsonui.dom.create(content));
	},

	setDomContent:function(domContent) {
		this.viewContainer.innerHTML = "";
		this.appendDomContent(domContent);
	},

	appendDomContent:function(domContent) {
		this.viewContainer.appendChild(domContent);
	},

	close:function() {
		if (jsonui.ModalForm.g_CurrentForm) {
			document.body.removeChild(this.container);
			jsonui.ModalForm.g_CurrentForm = null;
		}
	}
});

jsonui.ModalForm.g_CurrentForm = null;
jsonui.ModalForm.close = function() {
	if (jsonui.ModalForm.g_CurrentForm) {
		jsonui.ModalForm.g_CurrentForm.close();
	}
}
/*

jsonui.factory.SelectInputFactory = {
	build:function(domNode,metadata) {
		var children = domNode.childNodes;
		for (var i=0;i<children.length;++i) {
			var child = children[i];
			if (child.tagName=='LABEL') {
				metadata.label = child;
			}
			else if (child.tagName=='SELECT') {
				metadata.select = child;
				this.touchSelect(child,metadata);
			}
			else if (child.tagName=='SPAN') {
				metadata.span = child;
			}
		}
	},

	touchSelect:function(select,metadata) {
		if (metadata.type=="hint" && metadata.messages) {
			select.hintMessages = metadata.messages;
			select.metadata = metadata;
			select.addEventListener('change',function(event) {
				var label = this.options[this.selectedIndex].value;
				var msg = this.hintMessages[label];
				if (msg) {
					this.metadata.span.innerHTML = msg;
				}
				else {
					this.metadata.span.innerHTML = "";
				}
			});
		}
		if (typeof(metadata.onchange)=='function') {
			select.onChangeCallback = metadata.onchange;
			select.addEventListener('change',function(event) {
				this.onChangeCallback(this.options[this.selectedIndex].value,this.selectedIndex);
			});
		}
	}
}

jsonui.factory.TextInputFactory = {
	build:function(domNode,metadata) {
		var children = domNode.childNodes;
		for (var i=0;i<children.length;++i) {
			var child = children[i];
			if (child.tagName=='LABEL') {
				this.touchLabel(child,metadata);
				metadata.label = child;
			}
			else if (child.tagName=='INPUT') {
				this.touchInput(child,metadata);
				metadata.input = child;
				child.checkField = this.checkField;
			}
			else if (child.tagName=='SPAN') {
				this.touchSpan(child,metadata);
				metadata.span = child;
			}
		}
	}
};



	touchLabel:function(labelNode,metadata) {

	},

	touchInput:function(inputNode,metadata) {
		inputNode.metadata = metadata;
		inputNode.factory = this;
		inputNode.addEventListener('change',function(event) {
			if (!this.metadata.passField) {
				if (this.metadata.passField = document.getElementById(metadata.repeatFieldId) ) {
					this.metadata.passField.repeatMetadata = this.metadata;
					this.metadata.passField.repeatFactory = this.factory;
					this.metadata.passField.addEventListener('change',function(event) {
						this.repeatFactory.checkField(this.repeatMetadata);
					})
				}
			}
			this.factory.checkField(this.metadata);
		});
	},

	touchSpan:function(spanNode,metadata) {

	},

	checkField:function(metadata) {
		var format = true;
		if (metadata.type=="email") {
			format = jsonui.utils.checkEmail(metadata.input.value);
		}
		else if (metadata.type=="number") {
			format = jsonui.utils.checkNumber(metadata.input.value);
		}
		else if (metadata.regexp) {
			var regexp = typeof(metadata.regexp)=="string" ? RegExp(metadata.regexp):metadata.regexp;
			format = jsonui.utils.checkFormat(metadata.input.value,regexp)
		}

		if (format) {
			metadata.span.innerHTML = "";
			metadata.span.className = jsonui.form.hintClass;
		}
		else {
			metadata.span.innerHTML = jsonui.dictionary.translate("Invalid format");
			metadata.span.className = jsonui.form.hintClass + " error";
		}

		var passField = null;
		if (metadata.type=="newpassword" && metadata.repeatFieldId && (passField=document.getElementById(metadata.repeatFieldId)) && metadata.input.value!=passField.value) {
			metadata.span.innerHTML = jsonui.dictionary.translate("Passwords does not match");
			metadata.span.className = jsonui.form.hintClass + "error";
			format = false;
		}
		if (metadata.notempty && metadata.input.value=="") {
			metadata.span.innerHTML = jsonui.dictionary.translate("This field is required");
			metadata.span.className = jsonui.form.hintClass + " error";
			format = false;
		}
		return format;
	}
}

jsonui.factory.CheckBoxFactory = {
	build:jsonui.factory.TextInputFactory.build,
	touchLabel:jsonui.factory.TextInputFactory.touchLabel,
	touchInput:jsonui.factory.TextInputFactory.touchInput,
	touchSpan:jsonui.factory.TextInputFactory.touchSpan,

	checkField:function(metadata) {
		//console.log(metadata);
		if (metadata.type=="required" && !metadata.input.checked) {
			metadata.span.innerHTML = metadata.message;
		}
		else if (metadata.type=="required" && metadata.input.checked) {
			metadata.span.innerHTML = "";
		}
	}
}

jsonui.factory.FormFactory = {
	build:function(domNode,metadata) {
		domNode.metadata = metadata;
		domNode.onSubmitFunction = this.checkFields;
		if (metadata.ajaxform) domNode.onsubmit = function(event) { return false; }
		domNode.addEventListener('submit',function(event) {
			this.onSubmitFunction(this,this.metadata);
		});
	},

	checkFields:function(form,metadata) {
		var inputs = form.getElementsByTagName("input");
		var select = form.getElementsByTagName("select");
		var textArea = form.getElementsByTagName("textarea");
		var status = true;
		for (var i=0;i<inputs.length;++i) {
			var input = inputs[i];
			if (typeof(input.checkField)=='function' && input.metadata) {
				status = status && input.checkField(input.metadata);
			}
		}
		//console.log(inputs);
		//console.log(select);
		//console.log(textArea);
		return status;
	}
}
*/
